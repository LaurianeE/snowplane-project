package com.example.snowplane;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.snowplane.Controller.FragmentData;
import com.example.snowplane.Controller.PagerAdapter;
import com.example.snowplane.Controller.SearchOACI;
import com.example.snowplane.Model.APICaller;
import com.example.snowplane.Model.Airport;
import com.example.snowplane.Model.VolleyCallBack;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import butterknife.BindView;
import butterknife.ButterKnife;


public class Home extends FragmentActivity {

    private final static String TAG = "HOME";

    //3 variables pour gerer le slide entre différents fragements
    private List<Fragment> fragments; //Contient les différents fragement qui devront être affiché

    //Pour lier les élements de la vue à des variables
    @BindView(R.id.viewpager) ViewPager pager;  //Element permettant de gerer le slide (présent dans le layout activity_home.xml)
    //  5 images affichées en haut de l'activité permettant de savoir sur quel fragment l'utilisateur est et combien il y en a
    @BindView(R.id.imgHome_homepage) ImageView ic_home ;
    @BindView(R.id.dot_frag_1) ImageView ic_dot1 ;
    @BindView(R.id.dot_frag_2) ImageView ic_dot2 ;
    @BindView(R.id.dot_frag_3) ImageView ic_dot3 ;
    @BindView(R.id.dot_frag_4) ImageView ic_dot4 ;
    ArrayList<ImageView> dots;


    private PagerAdapter mPagerAdapter; // Gère l'affichage du bon fragment
    private int indice;

    //Pour gerer le faite que l'on puisse actualiser la recherche en reclicant sur le bouton de la page d'accueil
    private boolean isSearchAlreadyDone;
    //Fragment de recherche d'aéroports
    private SearchOACI fragHome= SearchOACI.newInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "fct On Create");

        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_home);

        ButterKnife.bind(this); //Permet de lier les élements de la vue à l'ativité

        isSearchAlreadyDone = false;

        //Listener permettant à l'utilisateur d'atteindre le fragement voulu en cliquant une des icones ne haut de l'écran
        ic_home.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                returnSearchFrag(0);
            }
        });
        ic_dot1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                returnSearchFrag(1);
            }
        });
        ic_dot2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                returnSearchFrag(2);
            }
        });
        ic_dot3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                returnSearchFrag(3);
            }
        });
        ic_dot4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                returnSearchFrag(4);
            }
        });

        //Liste de ces 4 icones, permettant de gérer facilement leur affichage en fonction du nombre de d'instances de FragmentData
        dots = new ArrayList<>();
        dots.add(ic_dot1);
        dots.add(ic_dot2);
        dots.add(ic_dot3);
        dots.add(ic_dot4);

        //Listener permettant de changer la couleur des points en haut de l'écran en fonction du fragment sur lequel
        //  est l'utilisateur
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                dotColorChange(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        //initialiser le pager
        this.initialisePaging();
    }



    /**
     * Initialise les fragments à afficher
     */
    private void initialisePaging() {
        Log.d(TAG, "fct initialisePaging");
        fragments = new Vector<Fragment>();
        fragments.add(fragHome);//Au lancement de l'application, seul la page de recherche d'OACI est affichée
        this.mPagerAdapter  = new PagerAdapter(super.getSupportFragmentManager(), fragments);
        pager.setAdapter(this.mPagerAdapter);

        updatePaging(); //Mettre à jour les pages affichées
    }

    /**
     * Met à jour les fragements à afficher
     */
    private void updatePaging(){
        Log.d(TAG, "fct update paging");
        mPagerAdapter.updatePageAdapter(fragments);
        mPagerAdapter.notifyDataSetChanged();

    }

    /**
     * Reset l'adapter et le lie à nouveau au viewpager
     */
    private void resetPaging(){
        this.mPagerAdapter  = new PagerAdapter(super.getSupportFragmentManager(), fragments);
        pager.setAdapter(this.mPagerAdapter);
    }

    /**
     * Fonction exécutée au clic du bouton de recherche du fragment "fragment_search_o_a_c_i.xml
     */
    public void SearchOACI(View view){

        //Affichage d'un toast pour indiquer à l'utilisateur que sa recherche est prise en compte
        //  car la récupération des informations par l'API peut durer quelques secondes
        Context context = getApplicationContext();
        CharSequence text = getResources().getString(R.string.toastForWait);
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, text, duration);
        toast.show();

        //Si une recherche à déjà été faite, supprime les instances de FragmentData
        Log.d(TAG, "fct search oaci");
        if (!isSearchAlreadyDone){// Si aucune recherche n'a été faite
            Log.d(TAG, "Pas de recherche");
            isSearchAlreadyDone= true;
        }else{
            Log.d(TAG, "Déjà une recherche");
            removeFrag();
        }

        //Récupère la liste des codes OACI
        List<String> lstOACIs = getOACICode();
        // Appels aux API et récupération des données
        APICaller.getSnowtams(this, lstOACIs, new VolleyCallBack() {
            //Methodes onSuccess() permettant au programme d'attendre les données de l'API avant de continuer le code
            @Override
            public void onSuccess() {
                APICaller.getAirports(getApplicationContext(), new VolleyCallBack() {
                    @Override
                    public void onSuccess() {
                        List<Airport> lstAeroport = APICaller.getAirportList();
                        Log.d(TAG, "list airport: "+ lstAeroport);

                        //Pour chaque aéroport trouvé, créer une nouvelle instance de FragmentData contenant les informations de cet aéroport
                        if (lstAeroport != null) {
                            for (Airport airport : lstAeroport) {
                                fragments.add(new FragmentData(airport));
                                Log.d(TAG, "add frag airport");
                            }

                            //Pour chaque aéroport trouvé, un point en haut de l'écran est affiché
                            int nb_aeroport = lstAeroport.size();
                            for (int i = 0; i < nb_aeroport; i++) {
                                dots.get(i).setVisibility(View.VISIBLE);
                            }
                        }

                        //Reset l'adapter et le veiwpager
                        resetPaging();

                        //Reaffiche sur le fragment de recherche les codes OACI que l'utilisateur vient de rechecher
                        SearchOACI frag = (SearchOACI) fragments.get(0);
                        frag.setSearchField(lstOACIs);

                        //Affichage du toster indiquant à l'utilisateur que les données ont été récupérées
                        Context context = getApplicationContext();
                        CharSequence text = getResources().getString(R.string.toastForGood);
                        int duration = Toast.LENGTH_SHORT;
                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                    }
                });
            }
        });

        //Reaffiche sur le fragment de recherche les codes OACI que l'utilisateur vient de rechecher
        SearchOACI frag = (SearchOACI) fragments.get(0);
        frag.setSearchField(lstOACIs);
    }

    /**
     * Fonction du bouton qui clean la research de la page d'accueil
     */
    public void CleanResearch(View view) { //Fonction exécutée au click du bouton recherche dans le fragment "fragment_search_o_a_c_i.xml

        //si une recherche à été faite, supprime de l'adapter les instances de FragmentData de l'adapter
        Log.d(TAG, " Fct clean");
        if(isSearchAlreadyDone){
            Log.d(TAG, "Clean à faire");
            isSearchAlreadyDone =false;
            removeFrag();
        }
        //Reset les champs de recherche
        SearchOACI frag = (SearchOACI) fragments.get(0);
        frag.resetField();

        //Met à jour l'adapter
        updatePaging();
        Log.d(TAG, "Clean fait");
    }

    /**
     * Fonction supprimant de la liste des fragments les instances de FragmentData
     */
    private void removeFrag(){
        Log.d(TAG, "fct remove frag");
        for(int i =fragments.size()-1 ; i>0;i--){
            fragments.remove(i);
        }
        Log.d(TAG, "lst frag après remove : " + fragments);
        //Rend invisibles les points en haut de l'écran
        for (ImageView dot : dots) {
            dot.setVisibility(View.GONE);
        }
        resetPaging();
    }

    /**
     * Fonction permettant à l'utilisateur d'atteindre le fragment qu'il souhaite en cliquant sur les icones en haut de l'écran
     * @param position
     */
    public void returnSearchFrag(int position){
        Log.d(TAG, "fct return search frag : " + position);

        pager.setCurrentItem(position);

        dotColorChange(position);
    }

    /**
     * Fonction permettant de changer la couleur des icones en haut de l'écran en fonction de l'icone sur
     * laquelle l'utilisateur a cliqué
     * @param position
     */
    public void dotColorChange(int position){
        switch (indice){
            case 0 :
                ic_home.setColorFilter(getResources().getColor(R.color.Blue, getTheme()));
                break;
            case 1 :
                ic_dot1.setColorFilter(getResources().getColor(R.color.Blue, getTheme()));
                break;
            case 2 :
                ic_dot2.setColorFilter(getResources().getColor(R.color.Blue, getTheme()));
                break;
            case 3 :
                ic_dot3.setColorFilter(getResources().getColor(R.color.Blue, getTheme()));
                break;
            case 4:
                ic_dot4.setColorFilter(getResources().getColor(R.color.Blue, getTheme()));
                break;
        }

        switch (position){
            case 0 :
                ic_home.setColorFilter(getResources().getColor(R.color.BlueGray, getTheme()));
                break;
            case 1 :
                ic_dot1.setColorFilter(getResources().getColor(R.color.BlueGray, getTheme()));
                break;
            case 2 :
                ic_dot2.setColorFilter(getResources().getColor(R.color.BlueGray, getTheme()));
                break;
            case 3 :
                ic_dot3.setColorFilter(getResources().getColor(R.color.BlueGray, getTheme()));
                break;
            case 4:
                ic_dot4.setColorFilter(getResources().getColor(R.color.BlueGray, getTheme()));
                break;
        }
        indice = position;
    }

    /**
     * Fonciton permettant de récupérer la liste des codes OACI en appellant la méthode du meme nom dans le Fragment Search_OACI
     * @return
     */
    public List<String> getOACICode(){
        Log.d(TAG, "fct getOACICode");
        //Le fragment à la position 0 de la liste fragment est toujours une instance de SearchOACI
        SearchOACI frag = (SearchOACI) fragments.get(0);
        List<String> lst = frag.getOACICode();
        Log.d(TAG, "liste OACI : "+ lst);
        return lst;
    }

}
