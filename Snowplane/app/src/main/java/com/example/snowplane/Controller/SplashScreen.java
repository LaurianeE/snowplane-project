package com.example.snowplane.Controller;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.example.snowplane.Home;
import com.example.snowplane.R;

public class SplashScreen extends AppCompatActivity {

    public final static String SplashScreen = "SplashScreen";

    private final int durationSS = 2000; //2 seconds
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Log.d(SplashScreen, "Lancement de l'application");
        //Lie la vue à cette activité
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        Thread splashscreen = new Thread() {
            public void run() {
                try {
                    //le Thread durera 2 secondes
                    sleep(durationSS);

                    //Puis lance l'activité suivante -> Ici Home.java
                    Intent i=new Intent(getBaseContext(), Home.class);
                    startActivity(i);

                    //Termine cette activité
                    Log.d(SplashScreen, "Ouverture de la page Home de l'application");
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        //Démarre le Thread
        splashscreen.start();

    }
}