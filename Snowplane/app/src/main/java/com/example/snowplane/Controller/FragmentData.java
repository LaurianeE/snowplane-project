package com.example.snowplane.Controller;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.fragment.app.Fragment;

import com.example.snowplane.ActivityMap;
import com.example.snowplane.Model.Airport;
import com.example.snowplane.R;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.button.MaterialButtonToggleGroup;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentData extends Fragment implements View.OnClickListener{

    private final static String TAG = "FragmentData";

    private Airport airport;
    private Context ctx;

    private MaterialButton button_map;

    public FragmentData(Airport airport) {
        super();
        this.airport = airport;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.snowtam_data, container, false);

        TextView codedSnowtam = viewGroup.findViewById(R.id.codedSnowtamText);
        TextView airportName = viewGroup.findViewById(R.id.airportName);
        TextView oaciText = viewGroup.findViewById(R.id.oaciText);
        TextView countryName = viewGroup.findViewById(R.id.countryName);
        TextView stateCode = viewGroup.findViewById(R.id.stateCode);
        TextView cityName = viewGroup.findViewById(R.id.cityName);
        TextView longitude = viewGroup.findViewById(R.id.longitude);
        TextView latitude = viewGroup.findViewById(R.id.latitude);

        codedSnowtam.setText(airport.getSnowtam().getCodedsnowtam());
        airportName.setText(airport.getName());
        oaciText.setText(airport.getSnowtam().getOaci());
        countryName.setText(airport.getCountryName());
        stateCode.setText(airport.getStateCode());
        cityName.setText(airport.getCityName());
        longitude.setText(Double.toString(airport.getLongitude()));
        latitude.setText(Double.toString(airport.getLatitude()));

        ctx = this.getContext();

        //Lie le bouton ayant pour id "goToMap" à cette insatnce de la classe Fragment data
        button_map = viewGroup.findViewById(R.id.goToMap);
        //Place un "OnClickListenter" sur ce bouton -> Voir fonction OnClick
        button_map.setOnClickListener(this);

        return viewGroup;
    }

    public String getName(){
        return this.airport.getName();
    }

    /**Au clic du bouton "button_map", lance l'activité affichant la carte, en lui donnant, via les Extras, les coordonnées de l'instance de l'aéroport concernée
     * @param v
     */
    @Override
    public void onClick(View v){
        Log.d(TAG, "fct on click");
        Intent intent = new Intent(ctx, ActivityMap.class);
        intent.putExtra("lat", this.airport.getLatitude());
        intent.putExtra("lon", this.airport.getLongitude());
        startActivity(intent);
    }
}
