package com.example.snowplane.Controller; /**
 *
 */

import android.util.Log;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.List;

/**
 * The <code>PagerAdapter</code> serves the fragments when paging.
 * @author mwho
 */
public class PagerAdapter extends FragmentPagerAdapter {

    private List<Fragment> fragments;

    /**
     * Initialise l'adapter à partir d'une liste de fragment
     * @param fm
     * @param frag
     */
    public PagerAdapter(FragmentManager fm, List<Fragment> frag) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.fragments = frag;
    }

    //Met à jour la liste de fragment
    public void updatePageAdapter(List<Fragment> fragments){
        this.fragments = fragments;
    }

    //Récupère le fragment de l'adapter à la position donnée en paramètre
    @Override
    public Fragment getItem(int position) {
        return this.fragments.get(position);
    }

    //Retourne la taille de l'adapter -> le nombre de fragments qu'il contient
    @Override
    public int getCount() {
        return this.fragments.size();
    }
}