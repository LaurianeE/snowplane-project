package com.example.snowplane.Model;

import java.util.ArrayList;

public class Runway {
    private String runwayNumber;
    private String clearedRunwayLenght;
    private String clearedRunwayWidth;
    private String conditions;
    private String meanDepth;
    private String brakingAction;
    private String criticalSnowBank;
    private String lightsObscured;
    private String furtherClearance;
    private String timeOfCompletion;
    private String taxiway;
    private String snowBanks;
}
