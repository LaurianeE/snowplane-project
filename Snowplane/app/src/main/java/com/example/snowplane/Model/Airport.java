package com.example.snowplane.Model;

public class Airport {
    private String name;
    private Snowtam snowtam;

    private String countryName;
    private String stateCode;

    private String cityName;

    private double longitude;
    private double latitude;

    public Airport(String stateCode) {
        setStateCode(stateCode);
        setSnowtam(new Snowtam("Vide", "Vide"));
        setLongitude(0);
        setCityName("Vide");
        setName("Vide");
        setCountryName("Vide");
        setLatitude(0);
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getStateCode() {
        return stateCode;
    }
    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }
    public double getLongitude() {
        return longitude;
    }
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
    public double getLatitude() {
        return latitude;
    }
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }
    public String getCountryName() {
        return countryName;
    }
    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }
    public String getCityName() {
        return cityName;
    }
    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
    public Snowtam getSnowtam() {
        return snowtam;
    }
    public void setSnowtam(Snowtam snowtam) {
        this.snowtam = snowtam;
    }
}
