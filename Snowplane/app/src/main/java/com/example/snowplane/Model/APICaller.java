package com.example.snowplane.Model;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.snowplane.Home;
import com.example.snowplane.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class APICaller {

    private final static String TAG = "API_Caller";

    // URL de l'API (en plusieurs morceaux pour les aéroports)
    private static String URLSnowtam = "https://applications.icao.int/dataservices/api/notams-realtime-list?api_key=b3440530-37e5-11eb-be35-e58c7c1172a1&format=json&criticality=1&locations=";

    private static String URLAirport1 = "https://applications.icao.int/dataservices/api/indicators-list?api_key=b3440530-37e5-11eb-be35-e58c7c1172a1&state=";
    private static String URLAirport2 = "&airports=";
    private static String URLAirport3 = "&format=json";

    private static int expectedSnowtams;
    private static int expectedAirports;

    // Liste des aéroports correspondants
    public static List<Airport> airportList = new ArrayList<>();
    public static List<Airport> getAirportList() {return airportList;}


    // Appel de l'API pour récupérer les Snowtams
    public static void getSnowtams(Context context, List<String> oacis, final VolleyCallBack callBack) {
        expectedSnowtams = oacis.size();
        if (airportList.size() > 0) airportList.clear();
        // Pour chaque OACI fourni :
        for (String oaci : oacis) {
            Log.d(TAG, "Boucle sur chaque OACI");
            // Complétion de l'URL de l'API
            String url = URLSnowtam + oaci;

            // Requête
            StringRequest request = new StringRequest(Request.Method.GET, url, response -> {

            // Lecture du fichier json contenant le snowtam
            /*String response = "";
            InputStream is = context.getResources().openRawResource(R.raw.uuee);
            BufferedReader r = new BufferedReader(new InputStreamReader(is));
            StringBuilder total = new StringBuilder();
            try {
                for (String line; (line = r.readLine()) != null; ) {
                    total.append(line).append('\n');
                }
                response = total.toString();
            } catch (Exception e) {
                Log.e("ReadFile", e.getMessage());
            }*/
                // Vérification que la réponse n'est pas vide
                if(response.length() > 2) {
                    //Récupération des informations
                    String stateCode = response.substring(response.indexOf("StateCode") + 13, response.indexOf("StateCode") + 16);

                    Log.d("Test", "StateCode : " + stateCode);
                    Airport airport = new Airport(stateCode);

                    int indexSnowtam = response.indexOf("SNOWTAM");
                    if (indexSnowtam >= 0) {
                        String snowtamString = response.substring(indexSnowtam, response.indexOf("location", indexSnowtam) - 8);
                        Log.d("Test", "Snowtam : " + snowtamString);

                        snowtamString = snowtamString.replace("\\n", "\n");
                        Snowtam snowtam = new Snowtam(snowtamString, "UUEE");
                        airport.setSnowtam(snowtam);
                    }
                    else {
                        Snowtam snowtam = new Snowtam("Aucun snowtam disponible", "UUEE");
                        airport.setSnowtam(snowtam);
                    }

                    // Ajout de l'aéroport nouvellement crée à la liste
                    APICaller.airportList.add(airport);
                }
                else expectedSnowtams--;

                if (airportList.size() == expectedSnowtams){
                    callBack.onSuccess();
                }
            }, error -> Log.e("APICallerRequest", error.toString()));

            Log.d(TAG, "request getsnowtam : " + request);

            // Ajout de la requête à la liste d'appel
            VolleySingleton.getInstance(context).addToRequestQueue(request);
        }
    }

    // Appel à l'API pour récupérer les informations sur l'aéroport
    public static void getAirports(Context context, final VolleyCallBack callBack) {
        expectedAirports = expectedSnowtams;
        final int[] nbAirports = {0};

        if (airportList.size() > 0) {
            // Pour chaque aéroport avec Snowtam :
            for (int i = 0; i < airportList.size(); i++) {

                Log.d(TAG, "boucle pour chaque Snowtam trouvé");

                final int index = i;
                // Construction de l'URL
                String url = URLAirport1 + airportList.get(i).getStateCode() + URLAirport2 + airportList.get(i).getSnowtam().getOaci() + URLAirport3;

                // Requête JSON pour faciliter la récupération des données
                StringRequest request = new StringRequest(Request.Method.GET, url, response -> {

                // Lecture du fichier json contenant le snowtam
                /*String response = "";
                InputStream is = context.getResources().openRawResource(R.raw.airport);
                BufferedReader r = new BufferedReader(new InputStreamReader(is));
                StringBuilder total = new StringBuilder();
                try {
                    for (String line; (line = r.readLine()) != null; ) {
                        total.append(line).append('\n');
                    }
                    response = total.toString();
                } catch (Exception e) {
                    Log.e("ReadFile", e.getMessage());
                }*/
                    // Vérification que la réponse n'est pas vide
                    if (response.length() > 2) {
                        //Récupération des informations
                        Log.d("Test", "Airport : " + response.length() + " --> " + response);

                        String countryName = response.substring(response.indexOf("countryName") + 15, response.indexOf("countryCode") - 8);
                        String name = response.substring(response.indexOf("airportName") + 15, response.indexOf("cityName") - 8);
                        String cityName = response.substring(response.indexOf("cityName") + 12, response.indexOf("latitude") - 8);
                        String latitude = response.substring(response.indexOf("latitude") + 11, response.indexOf("longitude") - 7);
                        String longitude = response.substring(response.indexOf("longitude") + 12, response.indexOf("airportCode") - 7);

                        Airport airport = airportList.get(index);
                        airport.setCountryName(countryName);
                        airport.setName(name);
                        airport.setCityName(cityName);
                        airport.setLatitude(Double.valueOf(latitude));
                        airport.setLongitude(Double.valueOf(longitude));

                        nbAirports[0]++;
                    }
                    else expectedAirports--;

                    if (nbAirports[0] == expectedAirports){
                        callBack.onSuccess();
                    }
                }, error -> Log.e("APICaller", error.toString()));

                Log.d(TAG, "request getAirport : " + request);

                // Ajout de la requête à la liste d'appel
                VolleySingleton.getInstance(context).addToRequestQueue(request);
            }
        }
    }
}
