package com.example.snowplane.Model;


import java.util.ArrayList;

public class Snowtam {
    private String codedsnowtam;

    private String oaci;
    private String date;
    private ArrayList<Runway> runways;
    private String parking;
    private String nextObservation;
    private String notes;

    public Snowtam(String snowtam, String oaci) {
        codedsnowtam = snowtam;
        this.oaci = oaci;
    }

    public String getCodedsnowtam() {
        return codedsnowtam;
    }
    public void setCodedsnowtam(String codedsnowtam) {
        this.codedsnowtam = codedsnowtam;
    }
    public String getOaci() {
        return oaci;
    }
    public String getDate() {
        return date;
    }
    public ArrayList<Runway> getRunways() {
        return runways;
    }
    public String getParking() {
        return parking;
    }
    public String getNextObservation() {
        return nextObservation;
    }
    public String getNotes() {
        return notes;
    }
}
