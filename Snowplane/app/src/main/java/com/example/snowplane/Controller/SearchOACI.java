package com.example.snowplane.Controller;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.snowplane.Model.BaseFragment;
import com.example.snowplane.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SearchOACI#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SearchOACI extends BaseFragment {

    private final static String TAG = "FRAG_Search_OACI";

    //Elements de la vue à lier avec cette classe
    @BindView(R.id.searchOACI)Button searchButton;
    @BindView(R.id.cleanSearch)Button cleanButton;
    @BindView(R.id.ic_add_field)ImageView img_add_oaci_field;
    @BindView(R.id.ic_del_field)ImageView img_del_oaci_field;
    @BindView(R.id.search_field_1) EditText field1;
    @BindView(R.id.search_field_2) EditText field2;
    @BindView(R.id.search_field_3) EditText field3;
    @BindView(R.id.search_field_4) EditText field4;

    private int nbFieldVisible;

    /**
     * Créer l'instance du fragment de recherche d'oaci -> Appelé 1 fois dans Home.java
     */
    public static SearchOACI newInstance() {
        Log.d(TAG, "new Instance");
        Bundle args = new Bundle();
        SearchOACI fragment = new SearchOACI();
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Récuperer le layout du fragement
     */
    @Override
    protected int getFragmentLayout() {
        Log.d(TAG, "fonction get fragment");
        return R.layout.fragment_search_o_a_c_i;
    }

    /**
     * Initialisation des variables et de la vue du fragement
     */
    @Override
    protected void configureDesign() {
        Log.d(TAG, "fonction configure design");
        //Par défaut, un seul champ de recherche est affiché
        nbFieldVisible = 1;

        //Gère les listener et l'affichage des bouton "+" et "-"
        //Le bouton "+" affiche les EditText, affiche le bouton "-" à 2 champs de recherche et disparait lorsqu'il y a 4 champs de recherche
        img_add_oaci_field.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                nbFieldVisible ++;
                Log.d(TAG, "nbFieldVisible : " + nbFieldVisible);
                switch (nbFieldVisible){
                    case 2 :
                        field2.setVisibility(View.VISIBLE);
                        img_del_oaci_field.setVisibility(View.VISIBLE);
                        break;
                    case 3 :
                        field3.setVisibility(View.VISIBLE);
                        break;
                    case 4 :
                        field4.setVisibility(View.VISIBLE);
                        img_add_oaci_field.setVisibility(View.GONE);
                        break;
                }
         }});
        //Le bouton "-" fait disparaitre les EditText, affiche le bouton "+" à 3 champs et disparait lorsqu'il y a 1 champ de recherche
        img_del_oaci_field.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.d(TAG, "nbFieldVisible : " + nbFieldVisible);
                switch (nbFieldVisible){
                    case 2 :
                        field2.setVisibility(View.GONE);
                        img_del_oaci_field.setVisibility(View.GONE);
                        break;
                    case 3 :
                        field3.setVisibility(View.GONE);
                        break;
                    case 4 :
                        field4.setVisibility(View.GONE);
                        img_add_oaci_field.setVisibility(View.VISIBLE);
                        break;
                }
                nbFieldVisible --;
            }});
    }

    /**
     * Met à jour le design du fragement -> Mettre à jour la vue de manière dynamique
     * Inutile ici
     */
    @Override
    protected void updateDesign() {
        Log.d(TAG, "fonction update design");

    }

    /**
     * Fonction permettant de réinitialiser les valeurs de tous les champs et de masquer les 3 derniers champs de recherche
     */
    public void resetField(){
        Log.d(TAG, "reset farg");
        field1.setText("");
        field2.setText("");
        field3.setText("");
        field4.setText("");
        field2.setVisibility(View.GONE);
        field3.setVisibility(View.GONE);
        field4.setVisibility(View.GONE);
        nbFieldVisible =1;
        updateDesign();
    }

    /**
     * Récupère les données de chaque champs de recherche affichés à l'écran et les ajoute à une liste qui sera retourné à la classe Home.java
     * @return liste de code OACI
     */
    public List<String> getOACICode(){
        Log.d(TAG, "fonction get codes OACI");
        List<String> lstCodeOACI = new ArrayList<>();
        int nbField = nbFieldVisible;
        Log.d(TAG, "nb field = "+ nbField);
        String valueField;
        switch (nbField){
            case 4 :
                valueField = field4.getText().toString();
                if (!valueField.equals("")){
                    lstCodeOACI.add(valueField.toUpperCase());
                }
                nbField --;

            case 3 :
                valueField = field3.getText().toString();
                if (!valueField.equals("")){
                    lstCodeOACI.add(valueField.toUpperCase());
                }
                nbField--;

            case 2 :
                valueField = field2.getText().toString();
                if (!valueField.equals("")){
                    lstCodeOACI.add(valueField.toUpperCase());
                }
                nbField --;
            case 1 :
                valueField = field1.getText().toString();
                if (!valueField.equals("")){
                    lstCodeOACI.add(valueField.toUpperCase());
                }

        }
        return lstCodeOACI;
    }

    /**
     * Initialise les champs de recherche à partir d'une liste de String
     * @param lstOACIs
     */
    public void setSearchField(List<String> lstOACIs){
        switch (lstOACIs.size()){
            case 4 :
                field4.setText(lstOACIs.get(3));
                field4.setVisibility(View.VISIBLE);
                img_add_oaci_field.setVisibility(View.GONE);
            case 3 :
                field3.setText(lstOACIs.get(2));
                field3.setVisibility(View.VISIBLE);
            case 2 :
                field2.setText(lstOACIs.get(1));
                field2.setVisibility(View.VISIBLE);
                img_del_oaci_field.setVisibility(View.VISIBLE);
            case 1 :
                field1.setText(lstOACIs.get(0));
        }
        nbFieldVisible = lstOACIs.size();
    }

}

