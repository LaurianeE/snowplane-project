# SnowPlane - Project
Projet réalisé par Camille CLINCHAMP et Lauriane GOURAUD

# Bibliographie
- https://developer.android.com/docs
- https://stackoverflow.com
- https://thepseudocoder.wordpress.com
- https://www.geeksforgeeks.org
- https://medium.com
- https://caster.io

# Clé de l'API pour récupérer les Snowtam :
Clés valides :
- b3440530-37e5-11eb-be35-e58c7c1172a1
~~9bfea9b0-37d2-11eb-a997-7989dc4bfe0b~~
~~b0c63f90-3717-11eb-b0a4-e784b456499d~~
~~f2a78ab0-28b5-11eb-bdf4-c10e3ab13326~~
~~69357430-34c3-11eb-b237-d5b1a7d0df92~~

# URL pour les Snowtam :
Exemple de requete (clé non valide) :
https://applications.icao.int/dataservices/api/notams-realtime-list?api_key=f2a78ab0-28b5-11eb-bdf4-c10e3ab13326&format=json&criticality=1&locations=UUEE

# URL pour les informations sur l'aéroport :
Exemple de requete (clé non valide) :
https://applications.icao.int/dataservices/api/indicators-list?api_key=f2a78ab0-28b5-11eb-bdf4-c10e3ab13326&state=RUS&airports=UUEE&format=json
